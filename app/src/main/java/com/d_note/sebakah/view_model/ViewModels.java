package com.d_note.sebakah.view_model;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.d_note.sebakah.AppController;
import com.d_note.sebakah.model.Message;
import com.d_note.sebakah.model.UserModel;
import com.d_note.sebakah.network.NetworkInteractions;
import com.d_note.sebakah.room.AppDatabase;

import javax.inject.Inject;

public class ViewModels extends AndroidViewModel {

    @Inject AppDatabase mAppDatabase;
    private NetworkInteractions mNetworkInteractions = new NetworkInteractions();


    public ViewModels(@NonNull Application application) {
        super(application);
    }

    public LiveData<UserModel> getLoginStatus(UserModel user) {
        LiveData<UserModel> mUserModelLiveData = null;
        mUserModelLiveData = mNetworkInteractions.doLoginUser(user);
        return mUserModelLiveData;
    }

    public String[] getTokens() {
        ((AppController) getApplication()).getAppComponent().inject(this);
        String[] tokens = new String[2];
        tokens[0] = mAppDatabase.userModel().getAccessToken();
        tokens[1] = mAppDatabase.userModel().getRefreshToken();
        return tokens;
    }

    public LiveData<String> getTokenStatus(String token, String refreshToken) {
        LiveData<String> statusLiveData = null;
        statusLiveData = mNetworkInteractions.validateToken(token, refreshToken);
        return statusLiveData;
    }
    public LiveData<Message> doSignUpUser(UserModel user){
        LiveData<Message> userLiveData = null;
        userLiveData = mNetworkInteractions.doSignUpUser(user);
        return userLiveData;
    }
}
