package com.d_note.sebakah.network;

import com.d_note.sebakah.model.Message;
import com.d_note.sebakah.model.UserModel;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface NetworkApi {

    @POST("user/signin")
    Call<UserModel> signin(@Body UserModel user);

    @GET("/validate")
    Call<Message> validateToken();

    @POST("/user/signup")
    Call<Message> signUp(@Body UserModel user);
    /*
    *  @POST("user/signup")
    Call<HashMap<String, String>> signup(@Body UserProfile loginUser);

    @POST("user/signin")
    Call<UserProfile> signin(@Body UserProfile signUpUser);

    @GET("jobs/available")
    Call<List<Jobs>> availableJobs();

    @POST("jobs/addjob")
    Call<Jobs> addJob(/*@Header("Authorization2") String refreshToken, @Body Jobs jobs);

    @GET("jobs/available/{userId}")
    Call<List<Jobs>> getMyJobs(@Path("userId") String userId);
*/
}
