package com.d_note.sebakah.network;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;

import com.d_note.sebakah.BuildConfig;
import com.d_note.sebakah.model.JobModel;
import com.d_note.sebakah.model.Message;
import com.d_note.sebakah.model.UserModel;

import java.io.IOException;
import java.util.List;

import okhttp3.Headers;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.d_note.sebakah.GlobalConstants.ERROR;
import static com.d_note.sebakah.GlobalConstants.FAILED;
import static com.d_note.sebakah.GlobalConstants.SUCCESS;
import static com.d_note.sebakah.GlobalConstants.VALID_TOKEN_CODE;

public class NetworkInteractions {

    Retrofit.Builder mBuilder;
    Retrofit mRetrofit;
    NetworkApi mApiClient;

    public NetworkInteractions() {
    }

    private static Retrofit getRetroInstance() {
        return new Retrofit.Builder()
                .baseUrl(BuildConfig.BaseUrl)
                .addConverterFactory(GsonConverterFactory.create()).build();
    }

    private static Retrofit getRetroInstanceWithHeader(final String token, final String refreshToken) {
        final Headers.Builder headers = new Headers.Builder();
        headers.add("Authorization:bearer " + token);
        headers.add("Authorization2:refresh " + refreshToken);
        OkHttpClient httpClient = new OkHttpClient.Builder()
                .addInterceptor(
                        new Interceptor() {
                            @Override
                            public okhttp3.Response intercept(Chain chain) throws IOException {
                                Request original = chain.request();

                                Request request = original.newBuilder()
                                        .addHeader("Authorization", "bearer " + token)
                                        .addHeader("Authorization2", "refresh " + refreshToken)
                                        .method(original.method(), original.body())
                                        .build();
                                return chain.proceed(request);
                            }
                        }).build();
        return new Retrofit.Builder()
                .baseUrl(BuildConfig.BaseUrl)
                .client(httpClient)
                .addConverterFactory(GsonConverterFactory.create()).build();
    }

    public static NetworkApi getApiService() {
        return getRetroInstance().create(NetworkApi.class);
    }

    public static NetworkApi getApiServiceWithHeader(String token, String refreshToken) {
        return getRetroInstanceWithHeader(token, refreshToken).create(NetworkApi.class);
    }

    public LiveData<List<JobModel>> getListOfJobs(String url) {
        return null;
    }

    public LiveData<List<JobModel>> getAllOfMyJobs(String url) {
        return null;
    }

    public LiveData<UserModel> doLoginUser(UserModel user) {
        mApiClient = getApiService();
        final MutableLiveData<UserModel> userModelMutableLiveData = new MutableLiveData<>();

        Call<UserModel> call = mApiClient.signin(user);
        call.enqueue(new Callback<UserModel>() {
            @Override
            public void onResponse(Call<UserModel> call, Response<UserModel> response) {
                UserModel userStatus;
                if (response.isSuccessful()) {
                    UserModel user = response.body();
                    user.setStatus("success");
                    userModelMutableLiveData.setValue(user);
                } else {
                    userStatus = new UserModel("unauth");
                    userModelMutableLiveData.setValue(userStatus);
                }
            }

            @Override
            public void onFailure(Call<UserModel> call, Throwable t) {
                UserModel userStatus = new UserModel("error");
                userModelMutableLiveData.setValue(userStatus);
            }
        });
        return userModelMutableLiveData;
    }

    public LiveData<Message> doSignUpUser(UserModel user) {
        mApiClient = getApiService();
        final MutableLiveData<Message> userModelMutableLiveData = new MutableLiveData<>();

        Call<Message> call = mApiClient.signUp(user);
        call.enqueue(new Callback<Message>() {
            @Override public void onResponse(Call<Message> call, Response<Message> response) {
                if (response.code() == 201) {
                    Message message = new Message();
                    message.setMessage("success");
                    userModelMutableLiveData.setValue(message);
                } else if (response.code() == 409) {
                    Message message = new Message();
                    message.setMessage("failed");
                    userModelMutableLiveData.setValue(message);
                } else {
                    Message message = new Message();
                    message.setMessage("error");
                    userModelMutableLiveData.setValue(message);
                }
            }

            @Override public void onFailure(Call<Message> call, Throwable t) {
                Message message = new Message();
                message.setMessage("error");
                userModelMutableLiveData.setValue(message);
            }
        });
        return userModelMutableLiveData;
    }

    public LiveData<String> validateToken(String token, final String refreshToken) {
        final MutableLiveData<String> stringMutableLiveData = new MutableLiveData<>();
        mApiClient = getApiServiceWithHeader(token, refreshToken);
        Call<Message> call = mApiClient.validateToken();
        call.enqueue(new Callback<Message>() {
            @Override public void onResponse(Call<Message> call, Response<Message> response) {
                if (response.code() == VALID_TOKEN_CODE) {
                    stringMutableLiveData.setValue(SUCCESS);
                } else {
                    stringMutableLiveData.setValue(FAILED);
                }
            }

            @Override public void onFailure(Call<Message> call, Throwable t) {
                stringMutableLiveData.setValue(ERROR);
            }
        });
        return stringMutableLiveData;
    }
}
