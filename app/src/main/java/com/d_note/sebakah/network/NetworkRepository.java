package com.d_note.sebakah.network;

import android.arch.lifecycle.LiveData;

import com.d_note.sebakah.model.JobModel;
import com.d_note.sebakah.model.UserModel;

import java.util.List;

public interface NetworkRepository {

    LiveData<List<JobModel>> getListOfJobs(String url);

    LiveData<List<JobModel>> getAllOfMyJobs(String url);

    LiveData<UserModel> doLoginUser(UserModel user);

    void doSignUpUser(String url, UserModel user);
}
