package com.d_note.sebakah.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity(tableName = "users")
public class UserModel {
    @PrimaryKey @NonNull
    @ColumnInfo(name = "_id")
    private String user_id;
    @ColumnInfo(name = "username")
    private String username;
    @ColumnInfo(name = "phone_number")
    private String phone_number;
    @ColumnInfo(name = "fullname")
    private String fullname;
    @ColumnInfo(name = "password")
    private String password;
    @ColumnInfo(name = "user_type")
    private String user_type;
    @ColumnInfo(name = "token")
    private String token;
    @ColumnInfo(name = "refreshToken")
    private String refreshToken;
    @ColumnInfo(name = "nationality")
    private String nationality;
    @ColumnInfo(name = "address")
    private String address;
    @ColumnInfo(name = "user_catagory")
    private String user_catagory;
    @ColumnInfo(name = "dob")
    private String dob;
    @ColumnInfo(name = "status")
    private String status;

    @Ignore
    public UserModel(String status) {
        this.status = status;
    }

    @Ignore
    public UserModel(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public UserModel() {
    }

    @Ignore
    public UserModel(String username, String fullname, String password) {
        this.username = username;
        this.fullname = fullname;
        this.password = password;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUser_catagory() {
        return user_catagory;
    }

    public void setUser_catagory(String user_catagory) {
        this.user_catagory = user_catagory;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUser_type() {
        return user_type;
    }

    public void setUser_type(String user_type) {
        this.user_type = user_type;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
