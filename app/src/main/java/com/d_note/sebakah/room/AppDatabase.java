package com.d_note.sebakah.room;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.d_note.sebakah.model.UserModel;

@Database(entities = UserModel.class, version = 1)
public abstract class AppDatabase extends RoomDatabase {

    public abstract UserDao userModel();

    private static AppDatabase DATABASE_INSTANCE;

    public static AppDatabase getInMemoryDatabase(Context context) {
        if (DATABASE_INSTANCE == null) {
            DATABASE_INSTANCE = Room.databaseBuilder(context
                    .getApplicationContext(), AppDatabase.class, "user")
                    .allowMainThreadQueries()
                    .build();
        }
        return DATABASE_INSTANCE;
    }

    public static void destroyDbInstance(Context context) {
        DATABASE_INSTANCE = null;
        context.getDatabasePath("user").delete();
    }
}
