package com.d_note.sebakah.room;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.d_note.sebakah.model.UserModel;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

@Dao
public interface UserDao {

    @Query("SELECT * FROM users")
    LiveData<List<UserModel>> getAll();

    @Query("SELECT token FROM users")
    String getAccessToken();

    @Query("SELECT refreshToken FROM users")
    String getRefreshToken();

    @Query("SELECT username FROM users")
    String getUserName();

    @Query("SELECT fullname FROM users")
    String getFullName();

    @Query("SELECT _id FROM users")
    String getId();

    @Query("SELECT phone_number FROM users")
    String getPhoneNUmber();


    @Insert(onConflict = REPLACE)
    void insertUser(UserModel userModel);

    @Query("UPDATE users SET token = :token WHERE _id = :userId")
    void insertUserToken(String token, String userId);

    @Query("UPDATE users SET refreshToken = :refreshToken WHERE _id = :userId")
    void insertUserRefreshToken(String refreshToken, String userId);
}