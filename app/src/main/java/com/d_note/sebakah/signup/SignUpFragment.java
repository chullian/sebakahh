package com.d_note.sebakah.signup;


import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.d_note.sebakah.BaseFragment;
import com.d_note.sebakah.R;
import com.d_note.sebakah.model.Message;
import com.d_note.sebakah.model.UserModel;
import com.d_note.sebakah.signin.LoginFragment;
import com.d_note.sebakah.view_model.ViewModels;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 */
public class SignUpFragment extends BaseFragment {


    @BindView(R.id.progress) ConstraintLayout mProgressView;
    @BindView(R.id.sign_up_image_view_dp) CircleImageView mProfileImageView;
    @BindView(R.id.sign_up_change_dp) FloatingActionButton mChangeDp;
    @BindView(R.id.sign_up_et_full_name) EditText mFullNameEt;
    @BindView(R.id.sign_up_et_user_name) EditText mUserNameEt;
    @BindView(R.id.sign_up_et_pass) EditText mPasswordEt;
    @BindString(R.string.internal_server_error) String internal_server_error;
    @BindString(R.string.empty_fields) String empty_fields;
    @BindString(R.string.user_already_there) String user_already_there;
    @BindView(R.id.toolbar) Toolbar mToolbar;

    ViewModels mViewModels;
    boolean viewsEnabled = true;
    private View mView;


    public SignUpFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_sign_up, container, false);
        ButterKnife.bind(this, mView);
        setHasOptionsMenu(true);
        ((AppCompatActivity) getActivity()).setSupportActionBar(mToolbar);
        mViewModels = ViewModelProviders.of(this).get(ViewModels.class);
        return mView;
    }

    @Override public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_done:
                onClickDone();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void onClickDone() {
        if (viewsEnabled) {
            enableViews(false);
            mProgressView.setVisibility(View.VISIBLE);
            LiveData<Message> userData;
            String fullname = mFullNameEt.getText().toString();
            final String username = mUserNameEt.getText().toString();
            final String password = mPasswordEt.getText().toString();
            UserModel user = new UserModel(username, fullname, password);
            userData = mViewModels.doSignUpUser(user);
            userData.observe(this, new Observer<Message>() {
                @Override public void onChanged(@Nullable Message message) {
                    switch (message.getMessage()) {
                        case "error":
                            showSnackBar(getView(), internal_server_error, 3000);
                            break;
                        case "failed":
                            showSnackBar(getView(), user_already_there, 3000);
                            break;
                        default:
                            getActivity().getSupportFragmentManager().beginTransaction()
                                    .replace(((ViewGroup) getView().getParent()).getId(), LoginFragment.newInstance(username, password))
                                    .commit();
                            break;
                    }
                }
            });
        }
    }

    /**
     * method to change the view editability
     *
     * @param makeEnable true if views are enabled
     */
    void enableViews(boolean makeEnable) {
        viewsEnabled = makeEnable;
        mProfileImageView.setEnabled(makeEnable);
        mChangeDp.setEnabled(makeEnable);
        mFullNameEt.setEnabled(makeEnable);
        mUserNameEt.setEnabled(makeEnable);
        mPasswordEt.setEnabled(makeEnable);
    }
}
