package com.d_note.sebakah;

import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;

public abstract class BaseFragment extends Fragment {
    public void showSnackBar(View view, String message, int length) {
        Snackbar.make(view, message, length).show();
    }

    @Override public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_done,menu);
    }
}
