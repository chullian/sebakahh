package com.d_note.sebakah;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.Toolbar;

import com.d_note.sebakah.dialogs.AlertDialog;
import com.d_note.sebakah.profile.MyProfile;
import com.d_note.sebakah.signin.LoginFragment;

public class MainActivity extends BaseActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
       /* Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);*/

        FragmentManager fragmentManager = getSupportFragmentManager();
      /*  DialogFragment newFragment = new AlertDialog();
        newFragment.show(fragmentManager,"dialog");*/
        fragmentManager.beginTransaction()
                .replace(R.id.container_main,new LoginFragment(), "Login")
                .commit();
    }
}
