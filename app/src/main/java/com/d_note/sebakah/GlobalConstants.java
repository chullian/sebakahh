package com.d_note.sebakah;

public class GlobalConstants {
    public static int RESULT_OK = 1;
    public static int RESULT_FAILED = 0;
    public static int RESULT_ERROR = -1;

    public static int VALID_TOKEN_CODE = 200;
    public static int INVALID_TOKEN_CODE = 401;

    public static String SUCCESS = "success";
    public static String FAILED = "failed";
    public static String ERROR = "error";
}
