package com.d_note.sebakah;

import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.d_note.sebakah.network.ConnectivityReceiver;

public class BaseActivity extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener {

    public void setTitle(String title){
        getSupportActionBar().setTitle(title);
    }

//    public void setTitleToolbarColor(int color){
//        getSupportActionBar().setTitle(title);
//    }

    public void setHomesUpEnable(){
        getSupportActionBar().setHomeButtonEnabled(true);
    }

    @Override protected void onResume() {
        super.onResume();
        AppController app = (AppController)getApplication();
        app.getInstance().setConnectivityListener(this);
    }

    @Override public void onNetworkConnectionChanged(boolean isConnected) {
        Toast.makeText(this, "Voilaaaa", Toast.LENGTH_SHORT).show();
    }
}
