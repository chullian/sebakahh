package com.d_note.sebakah;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.view.View;

import com.d_note.sebakah.network.ConnectivityReceiver;
import com.d_note.sebakah.view_model.ViewModels;

import butterknife.BindView;
import butterknife.ButterKnife;


public class LauncherActivity extends BaseActivity {


    ViewModels mViewModel;
    LiveData<String> mStringLiveData;
    @BindView(R.id.progress) ConstraintLayout mProgressView;
    @BindView(R.id.connection_lost) ConstraintLayout mNetworkLost;

    @Override protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launcher);
        ButterKnife.bind(this);
        if (!ConnectivityReceiver.isConnected()) {
            mNetworkLost.setVisibility(View.VISIBLE);
            mProgressView.setVisibility(View.GONE);
        } else {
            mViewModel = ViewModelProviders.of(this).get(ViewModels.class);
            String[] tokens = mViewModel.getTokens();
            String token = tokens[0];
            String refreshToken = tokens[1];
            if (token != null || refreshToken != null) {
                mStringLiveData = mViewModel.getTokenStatus(token, refreshToken);
            }
            mStringLiveData.observe(this, new Observer<String>() {
                @Override public void onChanged(@Nullable String s) {
                    switch (s) {
                        case "error":
                            mNetworkLost.setVisibility(View.VISIBLE);
                            mProgressView.setVisibility(View.GONE);
                            break;
                        case "failed":
                            Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                public void run() {
                                    mProgressView.setVisibility(View.VISIBLE);
                                    mNetworkLost.setVisibility(View.GONE);
                                    startActivity(new Intent(LauncherActivity.this, MainActivity.class));
                                    finish();
                                }
                            }, 1500);
                            break;
                        default:
                            mProgressView.setVisibility(View.VISIBLE);
                            mNetworkLost.setVisibility(View.GONE);
                            //TODO startActivity(new Intent(LauncherActivity.this, HomeActivity.class));
                            startActivity(new Intent(LauncherActivity.this, MainActivity.class));
                            finish();
                    }
                }
            });
        }
    }
}
