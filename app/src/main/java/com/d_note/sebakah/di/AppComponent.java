package com.d_note.sebakah.di;

import com.d_note.sebakah.AppController;
import com.d_note.sebakah.LauncherActivity;
import com.d_note.sebakah.di.AppModule;
import com.d_note.sebakah.room.AppDatabase;
import com.d_note.sebakah.signin.LoginFragment;
import com.d_note.sebakah.view_model.ViewModels;

import javax.inject.Singleton;

import dagger.Component;

@Singleton @Component(modules = AppModule.class)
public interface AppComponent {

    void inject(AppController appController);
    void inject(LoginFragment loginFragment);
    void inject(LauncherActivity launcherActivity);
    void inject(ViewModels viewModels);
}
