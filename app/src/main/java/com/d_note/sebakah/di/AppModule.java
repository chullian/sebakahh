package com.d_note.sebakah.di;

import android.content.Context;
import android.content.SharedPreferences;

import com.d_note.sebakah.AppController;
import com.d_note.sebakah.room.AppDatabase;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {
    private final AppController mAppController;

    public AppModule(AppController appController) {
        mAppController = appController;
    }

    @Provides @Singleton
    Context providesContext(){
        return mAppController;
    }

    @Provides @Singleton
    SharedPreferences providesSharedPreferences(Context app){
        return app.getSharedPreferences("User",Context.MODE_PRIVATE);
    }

    @Provides @Singleton
    AppDatabase providesAppDatabase(Context app){
        return AppDatabase.getInMemoryDatabase(app);
    }

}
