package com.d_note.sebakah;

import android.app.Application;

import com.d_note.sebakah.di.AppComponent;
import com.d_note.sebakah.di.AppModule;
import com.d_note.sebakah.di.DaggerAppComponent;
import com.d_note.sebakah.network.ConnectivityReceiver;


public class AppController extends Application {

    private static AppController mInstance;

    public static synchronized AppController getInstance() {
        return mInstance;
    }
    AppComponent mAppComponent;
    @Override
    public void onCreate() {
        super.onCreate();
        mAppComponent = DaggerAppComponent.builder().appModule(new AppModule(this)).build();
        mAppComponent.inject(this);
        mInstance = this;

    }

    public AppComponent getAppComponent() {
        return mAppComponent;
    }

    public void setConnectivityListener(ConnectivityReceiver.ConnectivityReceiverListener listener) {
        ConnectivityReceiver.sConnectivityReceiverListener = listener;
    }
}