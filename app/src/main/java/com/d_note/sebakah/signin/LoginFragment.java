package com.d_note.sebakah.signin;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.d_note.sebakah.AppController;
import com.d_note.sebakah.BaseFragment;
import com.d_note.sebakah.R;
import com.d_note.sebakah.home.HomeActivity;
import com.d_note.sebakah.model.UserModel;
import com.d_note.sebakah.room.AppDatabase;
import com.d_note.sebakah.signup.SignUpFragment;
import com.d_note.sebakah.view_model.ViewModels;

import javax.inject.Inject;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A placeholder fragment containing a simple view.
 */
public class LoginFragment extends BaseFragment {

    @Inject
    SharedPreferences mSharedPreferences;
    @Inject AppDatabase mAppDatabase;
    @BindView(R.id.progress) ConstraintLayout mProgressView;
    @BindView(R.id.login_button_login)
    Button login;
    @BindView(R.id.login_button_sign_up)
    Button signUp;
    @BindView(R.id.login_edit_text_password)
    EditText password;
    @BindView(R.id.login_edit_text_username)
    EditText username;
    @BindString(R.string.unauth_login)
    String unauthAccess;
    @BindString(R.string.empty_fields)
    String empty_fields;

    @BindString(R.string.error_login)
    String errorLogin;
    ViewModels mLoginViewModel;
    LiveData<UserModel> mUserModelLiveData;

    private View mView;
    private boolean viewsEnabled = true;

    public LoginFragment() {
    }

    public static LoginFragment newInstance(String username, String password) {
        LoginFragment loginFragment = new LoginFragment();
        Bundle args = new Bundle();
        args.putString("username", username);
        args.putString("password", password);
        loginFragment.setArguments(args);
        return loginFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_login, container, false);
        ButterKnife.bind(this, mView);
        ((AppController) getActivity().getApplication()).getAppComponent().inject(this);
        mLoginViewModel = ViewModelProviders.of(this).get(ViewModels.class);
        if (getArguments() != null) {
            username.setText(getArguments().getString("username"));
            password.setText(getArguments().getString("password"));
        }
        return mView;
    }

    @OnClick(R.id.login_button_login)
    public void onLoginClicked() {
        enableViews(false);
        mProgressView.setVisibility(View.VISIBLE);
        if (!TextUtils.isEmpty(username.getText().toString()) && !TextUtils.isEmpty(password.getText().toString())) {
            final UserModel user = new UserModel(username.getText().toString(), password.getText().toString());
            mUserModelLiveData = mLoginViewModel.getLoginStatus(user);
            mUserModelLiveData.observe(this, new Observer<UserModel>() {
                @Override
                public void onChanged(@Nullable UserModel userModel) {
                    switch (userModel.getStatus()) {
                        case "error":
                            showSnackBar(getView(), errorLogin, 3000);
                            break;
                        case "unauth":
                            showSnackBar(getView(), unauthAccess, 3000);
                            break;
                        default:
                            mAppDatabase.userModel().insertUser(userModel);
                            startActivity(new Intent(getContext(), HomeActivity.class));
                            getActivity().finish();
                            break;
                    }
                }
            });
        } else {
            showSnackBar(getView(), empty_fields, 3000);
        }
    }

    @OnClick(R.id.login_button_sign_up)
    public void onSignUpClicked(View signUpButtonView) {
        getActivity().getSupportFragmentManager().beginTransaction()
                .replace(((ViewGroup) getView().getParent()).getId(), new SignUpFragment())
                .addToBackStack("login")
                .commit();
    }


    /**
     * method to change the view editability
     *
     * @param makeEnable true if views are enabled
     */
    void enableViews(boolean makeEnable) {

        viewsEnabled = makeEnable;
        login.setEnabled(makeEnable);
        signUp.setEnabled(makeEnable);
        password.setEnabled(makeEnable);
        username.setEnabled(makeEnable);
    }
}
